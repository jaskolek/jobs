<?php
return [
    'jaskolek' => [
        'cron' => [
            'scheduleAhead' => 60,
            'scheduleLifetime' => 60,

            'jobs' => [
            ],

            'jobs_factories' => [
            ],

            'manager' => [

            ]
        ],
    ],

    //Doctrine configuration
    'doctrine' => [
        'driver' => [
            'jaskolek_cron_entities' => [
                'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
                'cache' => 'array',
                'paths' => [__DIR__ . '/../src/Jaskolek/Cron/Entity']
            ],

            'orm_default' => [
                'drivers' => [
                    'Jaskolek\Cron\Entity' => 'jaskolek_cron_entities'
                ]
            ]
        ]
    ],

    'router' => [
        'routes' => [
            'jaskolek-cron' => [
                'type' => 'literal',
                'options' => [
                    'route' => '/cron',
                    'defaults' => [
                        'controller' => 'Jaskolek\Cron\Controller\Cron'
                    ]
                ],
                'may_terminate' => false,
                'child_routes' => [
                    'run' => [
                        'type' => 'segment',
                        'options' => [
                            'route' => '/run/:jobId',
                            'constraints' => [
                                'jobId' => "[0-9A-Za-z/%]+"
                            ],
                            'defaults' => [
                                'action' => 'run-job'
                            ]
                        ]
                    ]
                ]
            ]
        ]
    ],
    'console' => [
        'router' => [
            'routes' => [
                'Jaskolek\Cron\Cron' => [
                    'options' => [
                        'route' => 'cron',
                        'defaults' => [
                            'controller' => 'Jaskolek\Cron\Controller\Cron',
                            'action' => 'cron'
                        ]
                    ]
                ],
                'Jaskolek\Cron\RunJob' => [
                    'options' => [
                        'route' => 'cron run <jobId>',
                        'defaults' => [
                            'controller' => 'Jaskolek\Cron\Controller\Cron',
                            'action' => 'run-job'
                        ]
                    ]
                ]
            ]
        ]
    ]
];