<?php
namespace Jaskolek\Cron;

use Jaskolek\Cron\Controller\CronController;
use Jaskolek\Cron\Service\CronService;
use Zend\Mvc\Controller\ControllerManager;
use Zend\ServiceManager\Config;
use Zend\ServiceManager\ServiceManager;

class Module
{
    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getControllerConfig()
    {
        return [
            'invokables' => [
            ],
            'factories' => [
                'Jaskolek\Cron\Controller\Cron' => function (
                    ControllerManager $controllerManager
                ) {
                    $serviceLocator = $controllerManager->getServiceLocator();
                    return new CronController(
                        $serviceLocator->get("Jaskolek\Cron\Service\Cron")
                    );
                }
            ]
        ];
    }

    public function getAutoloaderConfig()
    {
        return [
            'Zend\Loader\StandardAutoloader' => [
                'namespaces' => [
                    __NAMESPACE__ => __DIR__ . '/src/' . str_replace(
                            "\\", DIRECTORY_SEPARATOR, __NAMESPACE__
                        ),
                ],
            ],
        ];
    }


    public function getServiceConfig()
    {
        return [
            'factories' => [
                'Jaskolek\Cron\Service\Cron' => function (ServiceManager $serviceLocator) {
                    $config = $serviceLocator->get("Config");
                    $cronConfig = $config['jaskolek']['cron'];
                    $cronPluginManager = $serviceLocator->get("Jaskolek\Cron\CronPluginManager");

                    return new CronService(
                        $cronPluginManager,
                        $serviceLocator->get("Doctrine\ORM\EntityManager"),
                        $cronConfig
                    );
                },
                'Jaskolek\Cron\CronPluginManager' => function(ServiceManager $serviceLocator){
                    $config = $serviceLocator->get("Config")["jaskolek"]["cron"]["manager"];
                    return new CronPluginManager(
                        new Config($config)
                    );
                }
            ]
        ];
    }
}
