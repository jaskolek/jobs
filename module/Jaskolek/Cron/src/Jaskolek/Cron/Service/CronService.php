<?php

namespace Jaskolek\Cron\Service;

use Cron\CronExpression;
use Doctrine\ORM\EntityManager;
use Jaskolek\Cron\Command\CommandInterface;
use Jaskolek\Cron\CronPluginManager;
use Jaskolek\Cron\Entity\CronTabEntry;
use Jaskolek\Cron\Entity\Repository\CronTabEntryRepository;
use Jaskolek\Cron\Job;
use Jaskolek\Cron\JobFactoryInterface;


class CronService
{

    /**
     * @var Job[]
     */
    private $_registeredJobs;

    /**
     * @var EntityManager
     */
    private $_entityManager;

    /**
     * @var CronTabEntryRepository
     */
    private $_cronTabEntryRepository;


    /**
     * @var CronPluginManager
     */
    private $_cronPluginManager;

    private $_scheduleAhead;
    private $_scheduleLifetime;

    public function __construct(
        CronPluginManager $cronPluginManager,
        EntityManager $entityManager,
        $config
    ) {
        $this->_cronPluginManager = $cronPluginManager;
        $this->_entityManager = $entityManager;
        $this->_cronTabEntryRepository = $entityManager->getRepository(
            "Jaskolek\Cron\Entity\CronTabEntry"
        );
        $this->_scheduleAhead = $config['scheduleAhead'];
        $this->_scheduleLifetime = $config['scheduleLifetime'];

        $this->_registeredJobs = [];

        foreach ($config['jobs'] as $jobName => $jobConfig) {
            /** @var CommandInterface $command */
            $command = $this->_cronPluginManager->get($jobConfig['command']);
            $job = new Job($command, $jobConfig['expression']);
            $this->registerJob($jobName, $job);
        }

        foreach ($config['jobs_factories'] as $jobFactory) {
            /**
             * @var JobFactoryInterface $factory
             */
            $factory = $this->_cronPluginManager->get($jobFactory);

            $jobs = $factory->createJobs();
            foreach ($jobs as $key => $job) {
                $this->registerJob($key, $job);
            }
        }
    }

    public function registerJob($name, Job $job)
    {
        $this->_registeredJobs[$name] = $job;
    }

    public function runJob($jobId)
    {
        $this->_registeredJobs[$jobId]->run();
    }

    public function schedule()
    {
        $startDatetime = new \DateTime();

        $pendingEntries = $this->_cronTabEntryRepository->findPendingEntries();
        /** @var CronTabEntry[] $entriesMap */
        $entriesMap = [];
        foreach ($pendingEntries as $entry) {

            $entry->setStatus(
                CronTabEntry::STATUS_MISSED
            ); // set all as missed - if found, then this will be replaced to pending again
            $entriesMap[$this->getUniqueJobId(
                $entry->getJobId(), $entry->getExecutionTime()
            )]
                = $entry;
        }

        $scheduleTo = clone $startDatetime;
        $scheduleTo->add(new \DateInterval("PT" . $this->_scheduleAhead . "M"));

        $scheduleFrom = clone $startDatetime;
        $scheduleFrom->sub(
            new \DateInterval("PT" . $this->_scheduleLifetime . "M")
        ); //convert to seconds;


        foreach ($this->_registeredJobs as $key => $job) {
            $cron = CronExpression::factory($job->getExpression());

            if ($cron->isDue($startDatetime)) {
                $uniqueId = $this->getUniqueJobId($key, $startDatetime);
                if (array_key_exists($uniqueId, $entriesMap) !== false
                ) { //found - set status to pending
                    $entriesMap[$uniqueId]->setStatus(
                        CronTabEntry::STATUS_PENDING
                    );
                }
            }


            $i = 0;
            while (($nextRun = $cron->getNextRunDate($startDatetime, $i))
                <= $scheduleTo) {

                $uniqueId = $this->getUniqueJobId($key, $nextRun);

                if (array_key_exists($uniqueId, $entriesMap) !== false
                ) { //found - set status to pending
                    $entriesMap[$uniqueId]->setStatus(
                        CronTabEntry::STATUS_PENDING
                    );
                } else {
                    //not found - add new entry
                    $newEntry = new CronTabEntry();
                    $newEntry->setJobId($key);
                    $newEntry->setExecutionTime($nextRun);
                    $newEntry->setStatus(CronTabEntry::STATUS_PENDING);
                    $entriesMap[$uniqueId] = $newEntry;
                    $this->_entityManager->persist($newEntry);
                }
                ++$i;
            }

            //check for missed entries
            $i = 0;
            while (($previousRun = $cron->getPreviousRunDate(
                    $startDatetime, $i
                )) >= $scheduleFrom) {
                $uniqueId = $this->getUniqueJobId($key, $previousRun);

                if (array_key_exists($uniqueId, $entriesMap) !== false
                ) { //found - set status to pending
                    $entriesMap[$uniqueId]->setStatus(
                        CronTabEntry::STATUS_PENDING
                    );
                }
                ++$i;
            }
        }


        //set entries as missed
        foreach ($entriesMap as $entry) {
            if ($entry->getStatus() == CronTabEntry::STATUS_MISSED) {
                $this->_entityManager->persist($entry);
            }
        }

        $this->_entityManager->flush();
    }

    public function getUniqueJobId($key, \DateTime $time)
    {
        return $key . $time->format("Y-m-d-H-i");
    }

    public function run()
    {
        while (($entry = $this->_cronTabEntryRepository->findEntryToRun())
            !== null) {
            try {
                $entry->setStatus(CronTabEntry::STATUS_RUNNING);
                $entry->setStartTime(new \DateTime());
                $this->_entityManager->persist($entry);
                $this->_entityManager->flush();

                if (array_key_exists($entry->getJobId(), $this->_registeredJobs)
                    === false
                ) {
                    throw new \Exception(
                        "Job " . $entry->getJobId() . " not found!"
                    );
                }

                $job = $this->_registeredJobs[$entry->getJobId()];
                $job->run();

                $entry->setStatus(CronTabEntry::STATUS_SUCCESS);
                $entry->setSuccessMessage($job->getSuccessMessage());
                $entry->setEndTime(new \DateTime());
                $this->_entityManager->persist($entry);
                $this->_entityManager->flush();
            } catch (\Exception $e) {
                if (!$this->_entityManager->isOpen()) {
                    throw $e;
                }

                $entry->setStatus(CronTabEntry::STATUS_ERROR);
                $entry->setEndTime(new \DateTime());
                $entry->setErrorMessage(
                    $e->getMessage() . "Trace:" . $e->getTraceAsString()
                );

                $this->_entityManager->persist($entry);
                $this->_entityManager->flush();
            }
        }
    }
}