<?php
/**
 * Created by PhpStorm.
 * User: jaskolek
 * Date: 2015-01-09
 * Time: 18:44
 */

namespace Jaskolek\Cron;


use Jaskolek\Cron\Command\CommandInterface;
use Jaskolek\Cron\JobFactory\JobFactoryInterface;
use Zend\ServiceManager\AbstractPluginManager;
use Zend\ServiceManager\Exception;

class CronPluginManager extends AbstractPluginManager{

    /**
     * Validate the plugin
     *
     * Checks that the filter loaded is either a valid callback or an instance
     * of FilterInterface.
     *
     * @param  mixed $plugin
     *
     * @return void
     * @throws Exception\RuntimeException if invalid
     */
    public function validatePlugin($plugin)
    {
        if ($plugin instanceof JobFactoryInterface or $plugin instanceof CommandInterface) {
            return;
        }

        throw new Exception\RuntimeException("\$plugin must be Jaskolek\Cron\JobFactory\JobFactoryInterface or Jaskolek\Cron\Command\CommandInterface. " . get_class($plugin) . " given.");
    }
}