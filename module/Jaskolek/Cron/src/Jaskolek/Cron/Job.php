<?php
/**
 * Created by PhpStorm.
 * User: jaskolek
 * Date: 2014-12-01
 * Time: 19:12
 */

namespace Jaskolek\Cron;


use Jaskolek\Cron\Command\CommandInterface;

class Job
{

    /** @var CommandInterface| */
    private $_command;
    private $_expression;
    private $_successMessage;

    public function __construct(CommandInterface $command, $expression)
    {
        $this->_command = $command;
        $this->_expression = $expression;
    }

    /**
     * @return mixed
     */
    public function getExpression()
    {
        return $this->_expression;
    }

    public function run()
    {
        $successMessage = $this->_command->run();
        $this->setSuccessMessage($successMessage);
    }

    /**
     * @return CommandInterface
     */
    public function getCommand()
    {
        return $this->_command;
    }

    /**
     * @param CommandInterface $command
     */
    public function setCommand($command)
    {
        $this->_command = $command;
    }

    /**
     * @return mixed
     */
    public function getSuccessMessage()
    {
        return $this->_successMessage;
    }

    /**
     * @param mixed $successMessage
     */
    public function setSuccessMessage($successMessage)
    {
        $this->_successMessage = $successMessage;
    }

} 