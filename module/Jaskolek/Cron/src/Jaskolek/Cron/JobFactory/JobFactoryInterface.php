<?php
/**
 * Created by PhpStorm.
 * User: jaskolek
 * Date: 2014-12-02
 * Time: 15:24
 */

namespace Jaskolek\Cron\JobFactory;


interface JobFactoryInterface
{
    /**
     * @return Job[]
     */
    public function createJobs();
} 