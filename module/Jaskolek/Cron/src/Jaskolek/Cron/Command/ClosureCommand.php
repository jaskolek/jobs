<?php
/**
 * Created by PhpStorm.
 * User: jaskolek
 * Date: 2014-12-03
 * Time: 16:44
 */

namespace Jaskolek\Cron\Command;


class ClosureCommand implements CommandInterface
{

    /**
     * @var \Closure
     */
    private $_closure;

    function __construct(\Closure $closure)
    {
        $this->_closure = $closure;
    }


    public function run()
    {
        return $this->_closure->__invoke();
    }
}