<?php
/**
 * Created by PhpStorm.
 * User: jaskolek
 * Date: 2014-12-01
 * Time: 19:41
 */

namespace Jaskolek\Cron\Command;


interface CommandInterface
{
    /**
     * @return string success message
     */
    public function run();
} 