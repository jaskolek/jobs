<?php
namespace Jaskolek\Cron\Entity\Repository;

use Doctrine\ORM\EntityRepository;
use Jaskolek\Cron\Entity\CronTabEntry;

class CronTabEntryRepository extends EntityRepository
{


    /**
     * @return CronTabEntry[]
     */
    public function findPendingEntries()
    {
        return $this->findBy(
            ["status" => CronTabEntry::STATUS_PENDING]
        );
    }

    /**
     * @return null|CronTabEntry
     */
    public function findEntryToRun()
    {
        $queryBuilder = $this->createQueryBuilder("e");
        $queryBuilder->select("e")
            ->andWhere("e.status LIKE :status")
            ->andWhere("e.executionTime <= :executionTime")
            ->orderBy("e.executionTime")
            ->setParameter("status", CronTabEntry::STATUS_PENDING)
            ->setParameter("executionTime", new \DateTime())
            ->setMaxResults(1);

//        var_dump($queryBuilder->getDQL());
//        exit;
        return $queryBuilder->getQuery()->getOneOrNullResult();
    }
}
