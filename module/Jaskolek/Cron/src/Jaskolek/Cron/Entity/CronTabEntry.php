<?php
/**
 * Created by PhpStorm.
 * User: jaskolek
 * Date: 2014-12-01
 * Time: 19:36
 */

namespace Jaskolek\Cron\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class CrontabEntry
 * @ORM\Entity(repositoryClass="Jaskolek\Cron\Entity\Repository\CronTabEntryRepository")
 * @ORM\Table(options={
 *     "charset":"utf8",
 *     "collate":"utf8_unicode_520_ci",
 * })
 */
class CronTabEntry
{

    const STATUS_PENDING = 'pending';
    const STATUS_RUNNING = 'running';
    const STATUS_SUCCESS = 'success';
    const STATUS_MISSED = 'missed';
    const STATUS_ERROR = 'error';


    /**
     * @var int
     * @ORM\GeneratedValue()
     * @ORM\Id()
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    protected $jobId;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime")
     */
    protected $executionTime;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    protected $status;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $startTime;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $endTime;

    /**
     * @var string
     * @ORM\Column(type="text", nullable=true)
     */
    protected $errorMessage;

    /**
     * @var string
     * @ORM\Column(type="text", nullable=true)
     */
    protected $successMessage;

    /**
     * @return \DateTime
     */
    public function getExecutionTime()
    {
        return $this->executionTime;
    }

    /**
     * @param \DateTime $executionTime
     */
    public function setExecutionTime($executionTime)
    {
        $this->executionTime = $executionTime;
    }


    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getJobId()
    {
        return $this->jobId;
    }

    /**
     * @param string $jobCode
     */
    public function setJobId($jobId)
    {
        $this->jobId = $jobId;
    }

    /**
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param string $status
     */
    public function setStatus($status)
    {
        if (!in_array(
            $status, [
                self::STATUS_PENDING,
                self::STATUS_ERROR,
                self::STATUS_MISSED,
                self::STATUS_RUNNING,
                self::STATUS_SUCCESS]
        )
        ) {
            throw new \InvalidArgumentException(
                $status . " is not valid status!"
            );
        }
        $this->status = $status;
    }

    /**
     * @return \DateTime
     */
    public function getEndTime()
    {
        return $this->endTime;
    }

    /**
     * @param \DateTime $endTime
     */
    public function setEndTime($endTime)
    {
        $this->endTime = $endTime;
    }

    /**
     * @return string
     */
    public function getErrorMessage()
    {
        return $this->errorMessage;
    }

    /**
     * @param string $errorMessage
     */
    public function setErrorMessage($errorMessage)
    {
        $this->errorMessage = $errorMessage;
    }

    /**
     * @return \DateTime
     */
    public function getStartTime()
    {
        return $this->startTime;
    }

    /**
     * @param \DateTime $startTime
     */
    public function setStartTime($startTime)
    {
        $this->startTime = $startTime;
    }

    /**
     * @return string
     */
    public function getSuccessMessage()
    {
        return $this->successMessage;
    }

    /**
     * @param string $successMessage
     */
    public function setSuccessMessage($successMessage)
    {
        $this->successMessage = $successMessage;
    }


}