<?php
/**
 * Created by PhpStorm.
 * User: jaskolek
 * Date: 2014-11-16
 * Time: 14:37
 */

namespace Jaskolek\Cron\Controller;


use Jaskolek\Cron\Service\CronService;
use Zend\Mvc\Controller\AbstractActionController;

class CronController extends AbstractActionController
{
    /**
     * @var CronService;
     */
    private $_cronService;


    public function __construct(CronService $cronService)
    {
        $this->_cronService = $cronService;
    }

    public function cronAction()
    {
        $this->_cronService->schedule();
        $this->_cronService->run();

        return false;
    }

    public function runJobAction()
    {
        $jobId = $this->params("jobId");

        $this->_cronService->runJob($jobId);
        return false;
    }
} 