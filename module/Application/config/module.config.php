<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2015 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

return [
    'doctrine' => [
        'driver' => [
            'Application' => [
                'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
                'cache' => 'array',
                'paths' => [
                    __DIR__ . '/../src/Application/Entity'
                ]
            ],

            'orm_default' => [
                'drivers' => [
                    'Application\Entity' => 'Application'
                ]
            ]
        ]
    ],

    'router' => [
        'routes' => [
            'application' => [
                'type' => 'Literal',
                'options' => [
                    'route' => '/',
                    'defaults' => [
                        '__NAMESPACE__' => 'Application\Controller',
                        'controller' => 'Index',
                        'action' => 'index',
                    ],
                ],
                'may_terminate' => true,
                'child_routes' => [
                    'controller_action' => [
                        'type' => 'Segment',
                        'options' => [
                            'route' => ':controller/:action[/:id]',
                            'constraints' => [
                                'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'id' => '[0-9]+',
                            ],
                            'defaults' => [
                                'id' => 0
                            ],
                        ],
                    ],
                    'action' => [
                        'type' => 'Segment',
                        'options' => [
                            'route' => ':action[/:id]',
                            'constraints' => [
                                'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'id' => '[0-9]+',
                            ],
                            'defaults' => [
                                'id' => 0
                            ],
                        ],
                    ],
                ],
            ],
        ],
    ],
    'service_manager' => [
        'invokables' => [
            'Application\JobOfferImporter\ElanceCom' => 'Application\JobOfferImporter\ElanceCom',
            'Application\JobOfferImporter\OferiaPl' => 'Application\JobOfferImporter\OferiaPl',
            'Application\JobOfferImporter\FreelancerCom' => 'Application\JobOfferImporter\FreelancerCom'
        ],
        'abstract_factories' => [
            'Zend\Cache\Service\StorageCacheAbstractServiceFactory',
            'Zend\Log\LoggerAbstractServiceFactory',
        ],
        'factories' => [
            'Application\Service\JobOffer' => 'Application\Factory\JobOfferServiceFactory',
        ],
        'aliases' => [
        ],
    ],

    'job_offer_service' => [
        'importers' => [
            'elanceCom' => [
                "importer" => "Application\JobOfferImporter\ElanceCom",
                "options" => [
                    "keywordList" => [
                        "scraping",
                        "scrape",
                        "data scraping",
                        "data entry",
                        "web scraping",
                        "bot",
                        "robot",
                    ]
                ]
            ],
            'oferiaPl' => [
                "importer" => "Application\JobOfferImporter\OferiaPl",
                "options" => [
                    "categoryList" => [
                        "programowanie/php"
                    ]
                ]
            ],
            'freelancerCom' => [
                "importer" => "Application\JobOfferImporter\FreelancerCom",
                "options" => [
                    'rowsetLength' => 20,
                    'skillIdList' => [92, 199],
                    'languageList' => ['en', 'pl']
                ]
            ]
//            'Application\JobOfferImporter\FreelancerCom',
//            'Application\JobOfferImporter\ODeskCom'
        ],
    ],

    'view_manager' => [
        'display_not_found_reason' => true,
        'display_exceptions' => true,
        'doctype' => 'HTML5',
        'not_found_template' => 'error/404',
        'exception_template' => 'error/index',
        'template_map' => [
            'layout/layout' => __DIR__ . '/../view/layout/layout.phtml',
            'application/index/index' => __DIR__ . '/../view/application/index/index.phtml',
            'error/404' => __DIR__ . '/../view/error/404.phtml',
            'error/index' => __DIR__ . '/../view/error/index.phtml',
        ],
        'template_path_stack' => [
            __DIR__ . '/../view',
        ],
        'strategies' => [
            'ViewJsonStrategy'
        ]
    ],
    // Placeholder for console routes
    'console' => [
        'router' => [
            'routes' => [
            ],
        ],
    ],
];
