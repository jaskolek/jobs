<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2015 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application\Controller;

use Application\Service\JobOfferService;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\JsonModel;
use Zend\View\Model\ViewModel;

class IndexController extends AbstractActionController
{
    /**
     * @var JobOfferService
     */
    protected $_jobOfferService;

    function __construct(JobOfferService $jobOfferService)
    {
        $this->_jobOfferService = $jobOfferService;
    }


    public function indexAction()
    {
        return new ViewModel();
    }

    public function listNewAction()
    {
        $jobOfferList = $this->_jobOfferService->findNewJobOfferList();

        $result = new JsonModel($jobOfferList);
        return $result;
    }

    public function hideAction()
    {
        $id = $this->params("id");

        $this->_jobOfferService->hideJobOffer($id);

        $result = new JsonModel(["success" => true]);
        return $result;
    }

    public function visitAction()
    {
        $id = $this->params("id");

        $this->_jobOfferService->visitJobOffer($id);

        $result = new JsonModel(["success" => true]);
        return $result;
    }
}
