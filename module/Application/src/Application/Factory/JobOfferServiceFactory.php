<?php
/**
 * Created by PhpStorm.
 * User: jaskolek
 * Date: 2015-03-04
 * Time: 11:02
 */
namespace Application\Factory;

use Application\Service\JobOfferService;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class JobOfferServiceFactory implements  FactoryInterface{

    /**
     * Create service
     *
     * @param ServiceLocatorInterface $serviceLocator
     * @return mixed
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $entityManager = $serviceLocator->get("Doctrine\ORM\EntityManager");
        $config = $serviceLocator->get("Config")["job_offer_service"];

        $importerNameList = $config['importers'];
        $importerList = [];
        foreach($importerNameList as $importerName => $data){
            $importer = $serviceLocator->get($data["importer"]);
            $importer->setOptions($data["options"]);
            $importerList[$importerName] = $importer;
        }

        return new JobOfferService($entityManager, $importerList);
    }
}