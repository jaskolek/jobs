<?php
/**
 * Created by PhpStorm.
 * User: jaskolek
 * Date: 2015-03-04
 * Time: 12:12
 */
namespace Application\Cron\Command;

use Application\Service\JobOfferService;
use Jaskolek\Cron\Command\CommandInterface;

class JobOfferImporter implements CommandInterface{

    protected $_jobOfferService;

    function __construct(JobOfferService $jobOfferService)
    {
        $this->_jobOfferService = $jobOfferService;
    }


    /**
     * @return string success message
     */
    public function run()
    {
        $this->_jobOfferService->importLatest();
    }
}