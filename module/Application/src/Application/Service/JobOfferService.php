<?php
/**
 * Created by PhpStorm.
 * User: jaskolek
 * Date: 2015-03-04
 * Time: 10:32
 */
namespace Application\Service;

use Application\Entity\JobOffer;
use Application\Entity\Repository\JobOfferRepository;
use Application\JobOfferImporter\JobOfferImporterInterface;
use Doctrine\ORM\EntityManager;
use Jaskolek\Utils\Bot\Bot;

/**
 * Class JobOfferService
 * @package Application\Service
 */
class JobOfferService
{

    /**
     * @var JobOfferImporterInterface[]
     */
    protected $_jobOfferImporterList;

    /**
     * @var EntityManager
     */
    protected $_entityManager;

    /**
     * @param EntityManager $entityManager
     * @param $jobOfferImporterList
     */
    function __construct(EntityManager $entityManager, $jobOfferImporterList)
    {
        $this->_entityManager = $entityManager;
        $this->_jobOfferImporterList = $jobOfferImporterList;
    }


    /**
     * @param array $order
     * @return array
     */
    public function findNewJobOfferList($order = ["publicationTime" => "DESC"])
    {
        /** @var JobOfferRepository $jobOfferRepository */
        $jobOfferRepository = $this->_entityManager->getRepository("Application\Entity\JobOffer");

        return $jobOfferRepository->findVisibleOffers($order);
    }

    /**
     * fetches latest job offers and saves them into database
     */
    public function importLatest()
    {
        /** @var JobOfferRepository $jobOfferRepository */
        $jobOfferRepository = $this->_entityManager->getRepository("Application\Entity\JobOffer");

        $offerList = [];
        /** @var JobOfferImporterInterface $jobOfferImporter */
        foreach ($this->_jobOfferImporterList as $importerName => $jobOfferImporter) {
            $importerOfferList = $jobOfferImporter->importLatest();
            foreach ($importerOfferList as $offer) {
                $offer->setImporterName($importerName);
                $offerList[$offer->getGuid()] = $offer;
            }
        }

        /** @var JobOffer $offer */
        foreach ($offerList as $offer) {
            if (!$jobOfferRepository->jobExists($offer)) {
                $this->_entityManager->persist($offer);
            }
        }
        $this->_entityManager->flush();
    }

    /**
     * @param $id
     * @return JobOffer
     */
    public function hideJobOffer($id)
    {
        /** @var JobOfferRepository $jobOfferRepository */
        $jobOfferRepository = $this->_entityManager->getRepository("Application\Entity\JobOffer");

        /** @var JobOffer $jobOffer */
        $jobOffer = $jobOfferRepository->find($id);

        $jobOffer->setVisible(false);
        $this->_entityManager->persist($jobOffer);
        $this->_entityManager->flush();

        return $jobOffer;
    }


    /**
     * @param $id
     * @return JobOffer
     */
    public function visitJobOffer($id)
    {
        /** @var JobOfferRepository $jobOfferRepository */
        $jobOfferRepository = $this->_entityManager->getRepository("Application\Entity\JobOffer");

        /** @var JobOffer $jobOffer */
        $jobOffer = $jobOfferRepository->find($id);

        $jobOffer->setVisited(true);
        $this->_entityManager->persist($jobOffer);
        $this->_entityManager->flush();

        return $jobOffer;
    }
}
