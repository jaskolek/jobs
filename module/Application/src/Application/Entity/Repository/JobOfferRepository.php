<?php
/**
 * Created by PhpStorm.
 * User: jaskolek
 * Date: 2015-03-04
 * Time: 11:25
 */
namespace Application\Entity\Repository;

use Application\Entity\JobOffer;
use Doctrine\ORM\EntityRepository;

class JobOfferRepository extends EntityRepository{

    public function jobExists(JobOffer $jobOffer)
    {
        $found = ($this->findOneBy(["guid" => $jobOffer->getGuid()]) !== null);

        return $found;
    }

    public function findVisibleOffers($order = null){
        return $this->findBy(["visible" => true], $order);
    }
}