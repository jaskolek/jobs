<?php
/**
 * Created by PhpStorm.
 * User: jaskolek
 * Date: 2015-03-03
 * Time: 19:33
 */

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class HourlyJob
 * @package Application\Entity
 * @ORM\Entity()
 */
class HourlyJobOffer extends JobOffer{

    /**
     * @var integer
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $priceMax;

    /**
     * @var integer
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $priceMin;

    /**
     * @return int
     */
    public function getPriceMax()
    {
        return $this->priceMax;
    }

    /**
     * @param int $priceMax
     */
    public function setPriceMax($priceMax)
    {
        $this->priceMax = $priceMax;
    }

    /**
     * @return int
     */
    public function getPriceMin()
    {
        return $this->priceMin;
    }

    /**
     * @param int $priceMin
     */
    public function setPriceMin($priceMin)
    {
        $this->priceMin = $priceMin;
    }

    function jsonSerialize()
    {
        $data = parent::jsonSerialize();
        $data["priceMax"] = $this->getPriceMax();
        $data["priceMin"] = $this->getPriceMin();
        $data["type"] = "HOURLY";

        return $data;
    }


}