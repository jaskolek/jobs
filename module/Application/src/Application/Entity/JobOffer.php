<?php
/**
 * Created by PhpStorm.
 * User: jaskolek
 * Date: 2015-03-03
 * Time: 19:18
 */

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class Job
 * @package Application\Entity
 * @ORM\Entity(repositoryClass="Application\Entity\Repository\JobOfferRepository")
 * @ORM\InheritanceType("SINGLE_TABLE")
 */
abstract class JobOffer implements \JsonSerializable
{

    /**
     * @var integer
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    protected $title;

    /**
     * @var string
     * @ORM\Column(type="text")
     */
    protected $description;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime")
     */
    protected $publicationTime;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    protected $url;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    protected $guid;


    /**
     * @var boolean
     * @ORM\Column(type="boolean")
     */
    protected $visited = false;

    /**
     * @var boolean
     * @ORM\Column(type="boolean")
     */
    protected $visible = true;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    protected $importerName;

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return \DateTime
     */
    public function getPublicationTime()
    {
        return $this->publicationTime;
    }

    /**
     * @param \DateTime $publicationTime
     */
    public function setPublicationTime($publicationTime)
    {
        $this->publicationTime = $publicationTime;
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param string $url
     */
    public function setUrl($url)
    {
        $this->url = $url;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getGuid()
    {
        return $this->guid;
    }

    /**
     * @param string $guid
     */
    public function setGuid($guid)
    {
        $this->guid = $guid;
    }

    /**
     * @return boolean
     */
    public function isVisited()
    {
        return $this->visited;
    }

    /**
     * @param boolean $visited
     */
    public function setVisited($visited)
    {
        $this->visited = $visited;
    }

    /**
     * @return boolean
     */
    public function isVisible()
    {
        return $this->visible;
    }

    /**
     * @param boolean $visible
     */
    public function setVisible($visible)
    {
        $this->visible = $visible;
    }

    /**
     * @return string
     */
    public function getImporterName()
    {
        return $this->importerName;
    }

    /**
     * @param string $importerName
     */
    public function setImporterName($importerName)
    {
        $this->importerName = $importerName;
    }

    /**
     * (PHP 5 &gt;= 5.4.0)<br/>
     * Specify data which should be serialized to JSON
     * @link http://php.net/manual/en/jsonserializable.jsonserialize.php
     * @return mixed data which can be serialized by <b>json_encode</b>,
     * which is a value of any type other than a resource.
     */
    function jsonSerialize()
    {
        return [
            "id" => $this->getId(),
            "title" => $this->getTitle(),
            "description" => $this->getDescription(),
            "publicationTime" => $this->getPublicationTime()->format('U'),
            "url" => $this->getUrl(),
            "guid" => $this->getGuid(),
            "visited" => $this->isVisited(),
            "visible" => $this->isVisible(),
            "importerName" => $this->getImporterName()
        ];
    }


}