<?php
/**
 * Created by PhpStorm.
 * User: jaskolek
 * Date: 2015-03-05
 * Time: 23:16
 */

namespace Application\JobOfferImporter;


use Application\Entity\FixedPriceJobOffer;
use Application\Entity\HourlyJobOffer;
use Application\Entity\JobOffer;
use GuzzleHttp\Client;

class FreelancerCom implements JobOfferImporterInterface{

    protected $_rowsetLenght;
    protected $_skillIdList;
    protected $_languageList;

    /**
     * @return JobOffer[]
     */
    public function importLatest()
    {
        //todo mozna dodac logowanie
        $url = "https://www.freelancer.com/ajax/table/project_contest_datatable.php?"
        ."sEcho=4&iColumns=29&sColumns=&iDisplayStart=0&iDisplayLength=" . $this->_rowsetLenght . "&iSortingCols=1&"
        ."iSortCol_0=6&sSortDir_0=desc&bSortable_0=false&bSortable_1=false&bSortable_2=false&"
        ."bSortable_3=true&bSortable_4=false&bSortable_5=false&bSortable_6=true&bSortable_7=false&"
        ."bSortable_8=true&bSortable_9=true&bSortable_10=false&bSortable_11=false&bSortable_12=false&"
        ."bSortable_13=false&bSortable_14=false&bSortable_15=false&bSortable_16=false&"
        ."bSortable_17=false&bSortable_18=false&bSortable_19=false&bSortable_20=false&"
        ."bSortable_21=false&bSortable_22=false&bSortable_23=false&bSortable_24=false&"
        ."bSortable_25=false&bSortable_26=false&bSortable_27=false&bSortable_28=false&"
        ."keyword=&featured=false&fulltime=false&nda=false&qualified=false&sealed=false&"
        ."urgent=false&guaranteed=false&highlight=false&private=false&top=false&type=p&"
        ."budget_min=false&budget_max=false&hourlyrate_min=false&hourlyrate_max=false&"
        ."skills_chosen=" . implode("%2C", $this->_skillIdList) ."&verified_employer=false&bidding_ends=N%2FA&bookmarked=false&"
        ."countries=false&languages=" . implode("%2C", $this->_languageList) . "&hourlyProjectDuration=false&advancedFilterPanelView=&"
        ."disablePushState=false&pushStateRoot=%2Fjobs&ul=en&uc=19&xpbonus_catIds=51%2C9%2C3%2C613%2C68%2C95%2C199%2C236&"
        ."jobIdEnable=on&status=open&m=true&_=" . time();

        $client = new Client();

        $data = $client->get($url)->json();
        $jobOfferList = [];
        foreach($data['aaData'] as $row){
            if($row[5] == "Fixed"){
                $jobOffer = new FixedPriceJobOffer();
            }else{
                $jobOffer = new HourlyJobOffer();
            }

            $url = "https://freelancer.com" . $row[21];
            $jobOffer->setDescription($row[2]);
            $jobOffer->setGuid($url);
            $jobOffer->setTitle($row[1]);
            $jobOffer->setPublicationTime(new \DateTime());
            $jobOffer->setUrl($url);
            $jobOffer->setPriceMax($row[26]['maxbudget_usd']);
            $jobOffer->setPriceMin($row[26]['minbudget_usd']);

            $jobOfferList[] = $jobOffer;
        }

        return $jobOfferList;
    }

    public function setOptions($options)
    {
        $this->_rowsetLenght = $options['rowsetLength'];
        $this->_skillIdList = $options['skillIdList'];
        $this->_languageList = $options['languageList'];
    }
}