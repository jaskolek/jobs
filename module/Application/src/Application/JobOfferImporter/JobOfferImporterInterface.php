<?php
/**
 * Created by PhpStorm.
 * User: jaskolek
 * Date: 2015-03-04
 * Time: 10:40
 */

namespace Application\JobOfferImporter;


use Application\Entity\JobOffer;

interface JobOfferImporterInterface {
    /**
     * @return JobOffer[]
     */
    public function importLatest();

    public function setOptions($options);
}