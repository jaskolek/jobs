<?php
/**
 * Created by PhpStorm.
 * User: jaskolek
 * Date: 2015-03-04
 * Time: 12:24
 */

namespace Application\JobOfferImporter;


use Application\Entity\FixedPriceJobOffer;
use Application\Entity\HourlyJobOffer;
use Application\Entity\JobOffer;
use GuzzleHttp\Client;

class ElanceCom implements JobOfferImporterInterface
{

    protected $_keywordList = [];

    /**
     * @return JobOffer[]
     */
    public function importLatest()
    {
        $client = new Client();
        $jobOfferList = [];
        foreach ($this->_keywordList as $keyword) {
            $url = "https://www.elance.com/r/rss/jobs/q-" . $keyword;
            $xml = $client->get($url)->xml();

            /** @var \SimpleXMLElement $item */
            foreach ($xml->channel->item as $item) {
                $description = $item->description;
                if (preg_match("|Fixed Price \((.*?)\)|ui", $description, $matches)) {
                    $jobOffer = new FixedPriceJobOffer();

                    $price = $this->parsePrice($matches[1]);
                    $jobOffer->setPriceMin($price['min']);
                    $jobOffer->setPriceMax($price['max']);
                } else if (preg_match("|Hourly \((.*?)\)|ui", $description, $matches)) {
                    $jobOffer = new HourlyJobOffer();

                    $price = $this->parsePrice($matches[1]);
                    $jobOffer->setPriceMin($price['min']);
                    $jobOffer->setPriceMax($price['max']);
                } else {
                    throw new \InvalidArgumentException("Can not get offer type!");
                }

                $jobOffer->setGuid((string)$item->guid);
                $jobOffer->setTitle((string)$item->title);
                $jobOffer->setUrl((string)$item->link);
                $time = new \DateTime($item->pubDate);
                $time->setTimezone(new \DateTimeZone("Europe/Berlin"));

//                $time->add(new \DateInterval("PT5H"));
                $jobOffer->setPublicationTime($time);

                $jobOffer->setDescription((string)$description);

                $jobOfferList[] = $jobOffer;
            }
        }
        return $jobOfferList;
    }

    protected function parsePrice($priceText){
        $priceText = str_replace(",", "", $priceText);
        $patterns = [
            "|Not Sure|ui",
            "|Less than \\$(?<max>[0-9]+)|ui",
            "|\\$(?<max>[0-9]+) or less|ui",
            "|\\$(?<min>[0-9]+) - \\$(?<max>[0-9]+)|ui",
            "|About \\$(?<both>[0-9]+)|ui",
        ];

        $price = [
            "min" => 0,
            "max" => 0
        ];
        foreach($patterns as $pattern){
            if( preg_match($pattern, $priceText, $matches) ){
                if(isset($matches["both"])){
                    $price["min"] = $matches["both"];
                    $price["max"] = $matches["both"];
                }
                if(isset($matches["min"])){
                    $price["min"] = $matches["min"];
                }
                if(isset($matches["max"])){
                    $price["max"] = $matches["max"];
                }
            }
        }

        return $price;
    }


    public function setOptions($options)
    {
        $this->_keywordList = $options["keywordList"];
    }

}