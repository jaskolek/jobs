<?php
/**
 * Created by PhpStorm.
 * User: jaskolek
 * Date: 2015-03-05
 * Time: 17:57
 */

namespace Application\JobOfferImporter;


use Application\Entity\FixedPriceJobOffer;
use Application\Entity\HourlyJobOffer;
use Application\Entity\JobOffer;
use GuzzleHttp\Client;
use GuzzleHttp\Event\CompleteEvent;
use Jaskolek\Utils\Bot\Bot;
use Jaskolek\Utils\Bot\RecordCache\FileRecordCache;
use Symfony\Component\DomCrawler\Crawler;

class OferiaPl implements JobOfferImporterInterface{

    protected $_categoryList;

    /**
     * @return JobOffer[]
     */
    public function importLatest()
    {
        $client = new Client();
        $bot = new Bot(null, new FileRecordCache("data/cache/oferia.pl.txt"));

        $jobOfferList = [];
        foreach($this->_categoryList as $category){
            $url = "http://oferia.pl/zlecenia/" . $category;

            $body = $client->get($url)->getBody()->getContents();
            $crawler = new Crawler($body);
            $jobOfferUrlList = $crawler->filter(".listing-item")->each(function(Crawler $crawler){
                $titleNode = $crawler->filter(".listing-order-name > a");
                $url = "http://oferia.pl" . $titleNode->attr("href");

                return $url;
            });

            $categoryJobOfferList = $bot->send($jobOfferUrlList, function(CompleteEvent $completeEvent){
                $crawler = new Crawler($completeEvent->getResponse()->getBody()->getContents());

                $url = $completeEvent->getRequest()->getUrl();
                $priceText = $crawler->filter(".orderMetaBudget")->text();
                $price = (int) $crawler->filter(".orderMetaBudget > strong")->text();
                $price = round($price * 0.27);
                if(preg_match("|całość|ui", $priceText)){
                    $jobOffer = new FixedPriceJobOffer();
                }else{
                    $jobOffer = new HourlyJobOffer();
                }

                $jobOffer->setPriceMax($price);
                $jobOffer->setPriceMin($price);
                $jobOffer->setGuid($url);
                $jobOffer->setUrl($url);

                $jobOffer->setTitle($crawler->filter(".cardOrderName")->text());
                $jobOffer->setDescription($crawler->filter("#user_field")->text());
                $jobOffer->setPublicationTime(new \DateTime());
                return [$jobOffer];
            });

            foreach($categoryJobOfferList as $categoryJobOffer){
                $jobOfferList[] = $categoryJobOffer;
            }
        }
        return $jobOfferList;
    }

    public function setOptions($options)
    {
        $this->_categoryList = $options["categoryList"];
    }
}