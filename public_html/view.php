
<!DOCTYPE html>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title></title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" href="/scraper/css/bootstrap.min.css">
    <style>
        body {
            padding-bottom: 20px;
        }

        div.well {
            height: 500px;
            overflow-y: scroll;
        }

        div.well.small {
            height: 240px;
            overflow-y: scroll;
        }

        #queue > li, #my-queue > li{
            cursor:pointer;
        }

    </style>
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
    <script src="https://code.jquery.com/jquery-2.1.3.min.js"></script>
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
</head>
<body>
<div class="container">
    <!-- Example row of columns -->
    <div class="row">
        <div class="col-md-8">
            <h3>Kolejka (<span id="queue-size">0</span>)</h3>
            <div class="well">
                <ul id="queue" class="list-group">
                    <li class="list-group-item list-group-item-primary">dsa</li>
                    <li class="list-group-item list-group-item-primary">dsa</li>
                    <li class="list-group-item list-group-item-primary">dsa</li>
                    <li class="list-group-item list-group-item-primary">dsa</li>
                    <li class="list-group-item list-group-item-primary">dsa</li>
                </ul>
            </div>
        </div>

        <div class="col-md-4">
            <h3>W trakcie</h3>
            <div class="well">
                <ul id="my-queue" class="list-group">
                </ul>
            </div>
        </div>
    </div>

    <hr/>

    <footer>
        <!--<p>&copy; jaskolek@gmail.com 2014</p>-->
    </footer>
</div>
<script>
</script>
<!-- /container -->
</body>
</html>